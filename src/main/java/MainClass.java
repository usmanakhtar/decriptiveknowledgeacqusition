import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;
import com.google.gson.Gson;

import edu.stanford.nlp.ling.CoreAnnotations;
import edu.stanford.nlp.ling.CoreLabel;
import edu.stanford.nlp.pipeline.Annotation;
import edu.stanford.nlp.pipeline.StanfordCoreNLP;
import edu.stanford.nlp.util.CoreMap;


import java.io.FileInputStream;
import java.util.*;

public class MainClass {

    public static Multimap<Integer, List<String>> sentenceList = ArrayListMultimap.create();

    public static void main(String[] arg) throws Exception {

        //List cui=new ArrayList<>();


        ArrayList<String>wordcollect=new ArrayList();

        Map<String,String> FinalResult = new HashMap<>();

          String array[]=new String[5];
        String array2[]=new String[5];


        wordcollect=stopWord(wordTokenize());

        System.out.println("word tokenize :"+wordcollect);
        System.out.println(sentenceList);

        String username="asimabbas";
        String Password="Andriod$555";


        RestTicketClient rs =new RestTicketClient(username,Password);



        String r=rs.getTgt();
        System.out.println( r);
        System.setProperty("apikey", r);
        System.setProperty("operation", "parents");
        System.setProperty("source", "SNOMEDCT_US");



/////////// search term

        SearchTermsTestCase searchuid = new SearchTermsTestCase();
        for (int k=0;k<wordcollect.size();k++) {
           searchuid.SearchTerm(wordcollect.get(k).toString());
            //System.out.println(wordcollect.get(k).toString());
        }

///////////// search semantic type id
        //System.out.println("CUI Size:"+SearchTermsTestCase.uiList.size());
        RetrieveCuiTestCase searchtypeid= new RetrieveCuiTestCase();
        for (int c=0;c<SearchTermsTestCase.uiList.size();c++) {

            //System.out.println("cui value:" +SearchTermsTestCase.uiList.get(c).toString());
            searchtypeid.RetrieveCui(SearchTermsTestCase.uiList.get(c).toString());


        }

     /////////////// combine all value entity type  sementic type, conecent
        int i=0;
        for (String term:wordcollect) {

            System.out.println("term:"+term);


            if (SearchTermsTestCase.cuiList.containsKey(term)) {

                System.out.println("");

                String w=SearchTermsTestCase.cuiList.get(term);
                System.out.println("w :  "+ w );
                System.out.print(term+"  :  |");
                String concept=SearchTermsTestCase.cuiList.get(term);

                        String entityType= RetrieveCuiTestCase.tpt.get(concept);
                       // System.out.print("Semantic Type1 :  " + searchtypeid.tpt.get(d));//+"Entity Type1:"+entity.get(0)); ///+"Entity Type:"+entity.get(0)
                        System.out.print(searchtypeid.tpt.get(concept));
                        FinalResult.put(term, entityType);

                System.out.println("");
            }
            i++;


        }

        wordContext(FinalResult,wordcollect);

        /*System.out.println("i:"+i);
        System.out.println(FinalResult);
        for (Map.Entry<String,String> entry : FinalResult.entrySet())
        {
            System.out.println(entry);
        }*/


    }


    public static void wordContext(Map<String, String> myfinal, ArrayList<String> myarray){

        //String[] dicProblem={"Disease or Syndrome","Sign or Symptom","Finding","Pathologic Function","Mental or Behavioral Dysfunction","Injury or Poisoning","Cell or Molecular Dysfunction","Congenital Abnormality","Acquired Abnormality","Neoplastic Process","Anatomic Abnormality","virus/becterium"};
    	String[] dicProblem={"Disease or Syndrome","Sign or Symptom","Finding","Pathologic Function","Mental or Behavioral Dysfunction","Injury or Poisoning","Cell or Molecular Dysfunction","Congenital Abnormality","Acquired Abnormality","Neoplastic Process","Anatomic Abnormality","virus/becterium"};
        String[] dicTreatment={"Therapeutic or Preventive Procedure","Organic Chemical", " Pharmacologic Substance", "Biomedical and Dental material" , "Antibiotic" , "Clinical Drug", " Steroid","Drug Delivery Device", "Medical Device"};
        String[] dicTest={"Tissue","Cell","Laboratory or Test Result","Laboratory Procedure","Diagnostic Procedure","Clinical Attribute","Body Substance"};

        //List cui=new ArrayList<>();
       

        ArrayList<String>wordcollect=new ArrayList();
        List<String> problem=Arrays.asList(dicProblem);
        List<String> treatment=Arrays.asList(dicTreatment);
        List<String> test=Arrays.asList(dicTest);
        /*ArrayList myarray=new ArrayList();
        myarray.add("30-year");
        myarray.add("old");
        myarray.add("male");
        myarray.add("diagnosed");
        myarray.add("with");
        myarray.add("depression");*/

       // Map<String,String> FinalResult = new HashMap<>();
       List<HashMap<Integer,String>> myMultiList = new ArrayList<>();
        HashMap<Integer, String> myMap = new HashMap<Integer, String>();
        /*FinalResult.put("old","Temporal Concept");
        FinalResult.put("depression","Mental or Behavioral Dysfunction");
        FinalResult.put("male","Organism Attribute");
        FinalResult.put("diagnosed","Diagnostic Procedure");*/
        final int[] count = {0};
        /*FinalResult.forEach((k, v) ->{*/
        for (Map.Entry<String,String> entry : myfinal.entrySet()){
           //System.out.println(k+":"+v);
            String v= entry.getValue();
            String k=entry.getKey();
            if (v.equals("Temporal Concept")){

                int word=myarray.indexOf(k);
                //System.out.println(word);
                if(word > 1){
                    System.out.println("Key if: " + k + ", Value: " + v);
                    String one=myarray.get(word-2)+" "+myarray.get(word-1)+" "+myarray.get(word);
                    String two=myarray.get(word)+" "+myarray.get(word+1)+" "+myarray.get(word+2);
                    String value = one.replaceAll("[^0-9]", "");

                    myMap.put(count[0],"key"+":"+two+","+" Value"+":"+value+", "+"operator"+":"+'=');
                   // myMultiList.add(myMap);

                }
                else {
                    System.out.println("Key else: " + k + ", Value: " + v);
                    String one=myarray.get(word-1)+" "+myarray.get(word);
                    String two=myarray.get(word)+" "+myarray.get(word+1);
                    String value = one.replaceAll("[^0-9]", "");
                    myMap.put(count[0],"key"+":"+two+","+" Value"+":"+value+", "+"operator"+":"+"=");
                    //myMultiList.add(myMap);
                }

            }

          else if (problem.contains(v)) {
                System.out.println("Key prob: " + k + ", Value: " + v);
                int word=myarray.indexOf(k);
                myMap.put(count[0],"key"+":"+myarray.get(word)+","+" Value"+":"+v+", "+"operator"+":"+"=");
                //myMultiList.add(myMap);
            }
              else if (treatment.contains(v)) {
                System.out.println("Key treat: " + k + ", Value: " + v);
                int word=myarray.indexOf(k);
                myMap.put(count[0],"key"+":"+myarray.get(word)+","+" Value"+":"+v+", "+"operator"+":"+"=");
               // myMultiList.add(myMap);


            }
            else if (test.contains(v)) {
                System.out.println("Key test: " + k + ", Value: " + v);
                int word=myarray.indexOf(k);
                myMap.put(count[0],"key"+":"+myarray.get(word)+","+" Value"+":"+v+", "+"operator"+":"+"=");
               // myMultiList.add(myMap);


            }

            count[0]++;
            System.out.println("map size:"+myMap.size()+": count:"+count[0]);


        }
        //myMultiList.add(myMap);
        //System.out.print("my map "+myMultiList);
        
        Gson gson = new Gson();
        // convert your list to json
           String myMultiList1 = gson.toJson(myMap);
        // print your generated json
           System.out.println("Json Format: " + myMultiList1);

    }

   /* public static  HashMap<String,String> FindWordContext(){

        String one=myarray.get(word-2)+" "+myarray.get(word-1)+" "+myarray.get(word);
        String two=myarray.get(word)+" "+myarray.get(word+1)+" "+myarray.get(word+2);


    }*/


    public static ArrayList<String> wordTokenize() {

       ArrayList<String> datalist=new ArrayList<>();
       int count=0;
       List<String> mysentlist=new ArrayList<>();
        String data = "There is strong evidence to treat a patient with a glucose level higher than six, fasting glucose more than 126 as diabetics patient";
        // String data = "A 55-year-old female has just come into the hospital. She has been diagnosed with high blood pressure. She exercises and eats right. You need to decide with your team whether she needs to be on beta-blocker or an ace-inhibitor";
        //String data="You just helped stitch up a patient’s knee laceration. He was clearly stressed, anxious and in pain. You had read somewhere that music would relax patients in such situations. You want to look for evidence to see if psychological stress in patients can be prevented or helped with the use of music therapy when they are undergoing treatments for their wounds.";
       //He would prefer to try alternative medicines rather than SSRI. You believe that his disorder would be improved with an SSRI. You believe that his disorder would be improved with SSRI, but he wants to know if ST. John’s Wort could be just as effective for reducing symptoms. 
       //String data="A 30-year old male is diagnosed with depression. He would prefer to try alternative medicines rather than SSRI.";
        data=data.replace(".","");
        Properties props = new Properties();
        props.setProperty("annotators", "tokenize, ssplit, pos, lemma, ner, parse, dcoref");
        //props.setProperty("annotators", "tokenize, ssplit, parse");
        StanfordCoreNLP pipeline = new StanfordCoreNLP(props);

        // read some text in the text variable
        String text = "...";

        // create an empty Annotation just with the given text
        Annotation document = new Annotation(data);

        // run all Annotators on this text
        pipeline.annotate(document);

        List<CoreMap> sentences = document.get(CoreAnnotations.SentencesAnnotation.class);
        System.out.println("sentences:  "+ sentences);
        for (CoreMap sentence : sentences) {
            System.out.println(sentence.toString());
            mysentlist.clear();
           // count+=1;
            for (CoreLabel token : sentence.get(CoreAnnotations.TokensAnnotation.class)) {
                // this is the text of the token
                String word = token.get(CoreAnnotations.TextAnnotation.class);
                System.out.println("word token"+word);
                datalist.add(word);
              // mysentlist.add(word);
            }
            //System.out.println("count:"+count);
           // sentenceList.put(count,mysentlist);
        }

        //System.out.println("sentence list"+sentenceList);
        return datalist;
    }


    public static ArrayList<String> stopWord(List list){

        ArrayList<String> stopword=new ArrayList<>();
        ArrayList<String> wordlist=new ArrayList<>();
        FileInputStream fileInputStream = null;
        try {
            //FileInputStream file=new FileInputStream("H:\\NLP\\Standford university\\stanford-corenlp-full-2016-10-31\\stanford-corenlp-full-2016-10-31\\patterns\\stopwords.txt");
        	//File file = new File("resources/stopwords.txt");
        	//FileInputStream file=new FileInputStream("resources/stopwords.txt");
        	
        	FileInputStream file=new FileInputStream("D:\\usman drive backup\\Project Work 2020\\DescriptiveKnowledgeAcqusition\\Version2\\NameEntityAnnotation\\stopwords.txt");
        	//FileInputStream file=new FileInputStream("resources/stopwords.txt");
        	
        	
        	byte b[] = new byte[file.available()];
            file.read(b);
            file.close();
            String data[] =new String(b).trim().split("\n");
            System.out.println(data);
            for (int i =0;i<data.length;i++){
                stopword.add(data[i].trim());
                //System.out.println(data[i].trim());
            }
            for (int j=0;j<list.size();j++){

                if(!(stopword.contains(list.get(j).toString().trim().toLowerCase()))){

                    wordlist.add((String) list.get(j));
                    System.out.print(list.get(j));
                }
            }


        }
        catch (Exception e){
            e.printStackTrace();
        }
        return wordlist;
    }
}