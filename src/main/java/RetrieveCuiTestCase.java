/*This example allows you to retrieve information about a known UMLS CUI.
Examples are at https://github.com/jayway/rest-assured/tree/master/examples/rest-assured-itest-java/src/test/java/com/jayway/restassured/itest/java
You can run this class as a Junit4 test case - be sure and put each of the arguments as VM arguments 
in your runtime configuration, such as -Dapikey=12345-abcdef -Did=C0018787
*/


import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;
import com.jayway.jsonpath.Configuration;
import com.jayway.jsonpath.JsonPath;
import com.jayway.jsonpath.spi.mapper.JacksonMappingProvider;
import com.jayway.restassured.RestAssured;
import com.jayway.restassured.response.Response;
import org.junit.Test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.jayway.restassured.RestAssured.given;


public class RetrieveCuiTestCase {
	public static String last;

	//String username = System.getProperty("username"); 
	//String password = System.getProperty("password");
	String apiKey = System.getProperty("apikey");
	//String id = System.getProperty("id");
	public static List tuiList=new ArrayList<>();
	public static List typeiList=new ArrayList<>();
	//public static Multimap<String, String> tpt = ArrayListMultimap.create();
	public static Map<String, String> tpt = new HashMap<>();
	public static Multimap<String, List> tptlist = ArrayListMultimap.create();
	//String id= (String) SearchTermsTestCase.uiList.get(0);
	String version = System.getProperty("version");
	RestTicketClient ticketClient = new RestTicketClient(apiKey);
	//get a ticket granting ticket for this session.
	//String tgt = ticketClient.getTgt();

	@Test
	public void RetrieveCui(String cui) throws Exception {
		
		    //if you omit the -Dversion parameter, use 'current' as the default.
		    version = System.getProperty("version") == null ? "current": System.getProperty("version");
		    String path = "/rest/content/"+version+"/CUI/"+cui;
			RestAssured.baseURI = "https://uts-ws.nlm.nih.gov";
	    	Response response =  given().log().all()
	                .request().with()
	                	.param("ticket", ticketClient.getST(apiKey))
	        	 .expect()
	       		 .statusCode(200)
	        	 .when().get(path);
	        	 //response.then().log().all();;     
	    	String output = response.getBody().asString();
			Configuration config = Configuration.builder().mappingProvider(new JacksonMappingProvider()).build();
			ConceptLite conceptLite = JsonPath.using(config).parse(output).read("$.result",ConceptLite.class);
				
 			//System.out.println(conceptLite.getUi()+": "+conceptLite.getName());
			//System.out.println("Semantic Type(s): "+ join(conceptLite.getSemanticTypes(),","));
			List<HashMap<String, Object>> entry = conceptLite.getSemanticTypes();
			String conceptName=conceptLite.getName();

			for(int i=0;i< entry.size();i++) {
				String semantictype = (String) entry.get(i).get("name");


				    //typeiList.add(semantictype);
					tuiList.add(semantictype);
					tpt.put(conceptName,semantictype);

			}
            //tptlist.put(conceptName,typeiList);



	}
}
