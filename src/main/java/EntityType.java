import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;
import com.jayway.jsonpath.Configuration;
import com.jayway.jsonpath.JsonPath;
import com.jayway.jsonpath.spi.mapper.JacksonMappingProvider;
import com.jayway.restassured.RestAssured;
import com.jayway.restassured.response.Response;
import org.junit.Test;

import java.util.*;

import static com.jayway.restassured.RestAssured.given;
import static org.apache.commons.lang.StringUtils.join;

public class EntityType {

    String apiKey = System.getProperty("apikey");
    String id = System.getProperty("id");
    String version = System.getProperty("version");
    //String source = System.getProperty("source");
    RestTicketClient ticketClient = new RestTicketClient(apiKey);
    //String last=RetrieveCuiTestCase.last;
    public static List entitylist=new ArrayList<>();
    public static Multimap<String, String> tpe = ArrayListMultimap.create();
    //String tgt = ticketClient.getTgt();

    @Test
    public void RetrieveEntity(String tui) throws Exception {

        //if you omit the -Dversion parameter, use 'current' as the default.
        version = System.getProperty("version") == null ? "current": System.getProperty("version");
        String path = "/rest/semantic-network/"+version+"/TUI/"+tui;
        RestAssured.baseURI = "https://uts-ws.nlm.nih.gov";
        Response response =  given().log().all()
                .request().with()
                .param("ticket", ticketClient.getST(apiKey))
                .expect()
                .statusCode(200)
                .when().get(path);
        //response.then().log().all();;
        String output = response.getBody().asString();
        Configuration config = Configuration.builder().mappingProvider(new JacksonMappingProvider()).build();
        ModelEntity code = JsonPath.using(config).parse(output).read("$.result",ModelEntity.class);

        //System.out.println(" ExpandedEntity: "+ code.getSemanticTypeGroup());
        HashMap<String,Object> entity=code.getSemanticTypeGroup();
        String en= (String) entity.get("expandedForm");
        String ui= code.getUi();
        //String sementictype=code.getName();
        //System.out.println(" entity type: "+ sementictype);
            if(en.equals("Disorders")) {
            tpe.put(tui, "Problem");
        }
        else if (en.equals("Chemicals & Drugs")|| en.equals("Devices")|| tui.equals("Therapeutic or Preventive Procedure")){
            tpe.put(tui,"Treatment");

        }
        else if (en.equals("Anatomy")||en.equals("Phenomena") || en.equals("Procedures")||en.equals("Physiology") && !tui.equals("Therapeutic or Preventive Procedure"))
        {

            tpe.put(tui,"Test");
        }
        else {

            tpe.put(tui,"NONE");
        }

            //entitylist.add(en);


  //System.out.println(en+"\t"+RetrieveCuiTestCase.semantictype+"\t"+SearchTermsTestCase.term);

    }

}
