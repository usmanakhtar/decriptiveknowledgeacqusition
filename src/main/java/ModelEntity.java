import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.HashMap;

@JsonIgnoreProperties({ "classType", "abbreviation","definition", "example", "nonHuman", "usageNote","treeNumber","classType","abbreviation","semanticTypeCount","childCount","name" })

public class ModelEntity {

    private HashMap<String,Object> semanticTypeGroup;
    private String name;
    private HashMap<String,String> mygroup;


    public HashMap<String, String> getMygroup() {
        return mygroup;
    }

    public void setMygroup(HashMap<String, String> mygroup) {
        this.mygroup = mygroup;
    }




    public String getUi() {
        return ui;
    }

    public void setUi(String ui) {
        this.ui = ui;
    }

    private String ui;




    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }



    public HashMap<String, Object> getSemanticTypeGroup() {
        return semanticTypeGroup;
    }

    public void setSemanticTypeGroup(HashMap<String, Object> semanticTypeGroup) {
        this.semanticTypeGroup = semanticTypeGroup;
    }








}
